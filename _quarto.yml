project:
  type: book

book:
  title: "Introduction to Quantum Computing"
  subtitle: "Lecture notes for the summer term 2024"
  author: 
    - name: Jannik Hellenkamp
    - name: Dominique Unruh
  downloads: [pdf, epub]
  version: 0.1.8
  search: true
  page-navigation: true
  site-url: https://qis.rwth-aachen.de/teaching/24ss/intro-quantum-computing/script/
  repo-url: https://git.rwth-aachen.de/j1/intro-qc/
  license: "CC BY-NC"
  date: today
  image: tmp-cover.png
  chapters:
    - index.qmd
    - part: quantumBasics.qmd
      chapters:  
        - introduction.qmd
        - probabilisticSystems.qmd
        - quantumSystems.qmd
        - observingSystems.qmd
        - partialObserving.qmd
        - compositeSystems.qmd
        - quantumCircutsKetNotation.qmd
    - part: quantumAlgorithms.qmd
      chapters:
        - bernsteinVazirani.qmd
        - shorsAlgorithm.qmd
        - groversAlgorithm.qmd
    - part: physicalBackground.qmd
      chapters:
        - physics.qmd
        - physicsToQC.qmd
        - ionBasedQC.qmd
        - universalGates.qmd
        - errorCorrection.qmd

bibliography: references.bib

format:
  html:
    theme: cosmo
    callout-appearance: simple
    default-image-extension: svg
  pdf:
    documentclass: scrreprt
    papersize: letter # Necessary for scrreprt!
    default-image-extension: pdf
    include-in-header: 
     text: |
        \usepackage{braket}
        \usepackage{arydshln}
        \usepackage{mathtools}
        \RedeclareSectionCommand[style=section]{chapter}
        \renewcommand{\part}[1]{\empty}

  epub: 
    callout-appearance: simple
    toc: true
    html-math-method: mathml # Requires an internet connection to compile